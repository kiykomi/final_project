# final_project

My final project files
##  My final project files

### Мой Jenkinsfile

```
#!groovy
//Тут я отключаю конкурентные сборки
properties([disableConcurrentBuilds()])

pipeline {
    agent {
        //Агент Jenkins, на котором буду делать всё
        label 'master'
        }
    options {
        //Задаю количество артефактов для хранения
      buildDiscarder logRotator(artifactNumToKeepStr: '10', numToKeepStr: '10')
      timestamps()
    }
    environment {
        //задаю переменные для дальнейшего использования
      IMAGE_BASE = 'kiykomi/pet-clinic'
      IMAGE_TAG = "v$BUILD_NUMBER"
      IMAGE_NAME = "${env.IMAGE_BASE}:${env.IMAGE_TAG}"
      IMAGE_NAME_LATEST = "${env.IMAGE_BASE}:latest"
      DOCKERFILE_NAME = "docker/toolbox/Dockerfile"
      //пустые переменные нужны для того, чтобы потом их использовать в коде.
      DEV_IP = ''
      PROD_IP = ''
    }
    stages {

        stage("building and testinf app") {
          stages {
            stage("Get sources from GitLab") {
              steps {
                  //качаю с гита, указанного для  хранения Jenkinsfile всё содержимое репы
                checkout scm
              }
            }
            
            stage("testing stage") {
              steps {
                dir ('source_code/spring-petclinic') {
                    //запускаю тест кода
                   sh 'sh mvnw test'
                   //сохраняю файл с репортом о тестах
                   sh 'sh mvnw surefire-report:report'
                   //читаю юнит-тесты
                   junit 'target/surefire-reports/TEST-*.xml'
                }
              }
            }
            
            stage("build artifact") {
              steps {
                  //задаю папку, в которой Jenkins будет выполнять действия
                dir ('source_code/spring-petclinic') {
                    //собираю  артефакт из исходников с пропуском тестов и прочих вещей
                   sh 'sh mvnw package -DskipTests'
                }
                dir ('.') {
                   echo "===========Copying artifact to docker folder============="
                   //копирую артефакт  в папку с докер файлом
                   sh 'cp source_code/spring-petclinic/target/*.jar docker/toolbox/app.jar'
                   echo "===========Archiving artifact for Jenkins============="
                   //помечаю  артефакты для Jenkins, чтобы увидеть их в результатах пайплайна
                   archiveArtifacts(artifacts: 'source_code/spring-petclinic/target/*.jar')
                   archiveArtifacts(artifacts: 'source_code/spring-petclinic/target/site/surefire-report.html')
                   echo "===========Artifact has copied to Docker folder and Archived for Jenkins============="
                }
              }
            }

          }
        }
        
        stage("docker login") {
            steps {
                echo "===========docker login============="
                //с помощью плагина Pipeline: AWS Steps авторизуюсь на  докерхабе
                withCredentials([usernamePassword(credentialsId: 'dockerhub_kiykomi', usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
                    sh """
                    //логинюсь
                    docker login -u $USERNAME -p $PASSWORD
                    """
                echo "===========docker login Success!============="
                }
            }
        }


        stage('Push images to registry') {
              steps {
                script {
                    //объявляю переменную
                  def dockerImage = docker.build("${IMAGE_NAME}", "-f ${DOCKERFILE_NAME} .")
                  //собираю образ докера
                  docker.withRegistry('', 'dockerhub_kiykomi') {
                      //пушу образ на докерхаб
                    dockerImage.push()
                    dockerImage.push("latest")
                  }
                  echo "Pushed Docker Image: ${IMAGE_NAME}"
                }

                //подчищаю  за докером все образы и контейнеры
                sh "docker rmi ${IMAGE_NAME} ${IMAGE_NAME_LATEST}"
                sh "docker image prune -a -f"
                //чищу  систему от мусора после сборки образов докера
                sh "docker system prune -a"
              }
        }
        
        stage('Build Environment with Terraform') {
          
          steps {
            checkout scm
            //перехожу в папку с терраформ
            dir ('terraform') {
                //логинюсь с помощью Pipeline: AWS Steps
              withAWS(credentials: 'AWS_CREDS', region: 'eu-central-1') {
                sh """
                // собственно поднимаю инфраструктуру
                terraform init
                terraform apply --auto-approve
                """
                script {
                    //  делаю запрос вывода из терраформ в переменные
                  DEV_IP = sh(returnStdout: true, script: "terraform output -raw dev_server_ip").trim()
                  PROD_IP = sh(returnStdout: true, script: "terraform output -raw prod_server_ip").trim()
                }
                // записываю переменные в хостс файл для Ansible
                writeFile (file: '../ansible/hosts.txt', text: '[dev]\n' + DEV_IP + '\n' + '[prod]\n' + PROD_IP + '\n')

              }
            }
          }
        }
        stage('Configuring Dev Environment with Ansible') {
          
          steps {
            dir ('ansible') {
                // запускаю плейбук с начтройками
              sh 'ansible-playbook devenv.yml'
            }
          }
          post {
              //отправляю репорт с адресом сервера в телегу
            always {
              withCredentials([string(credentialsId: 'botSecret', variable: 'TOKEN'), string(credentialsId: 'chatId', variable: 'CHAT_ID')]) {
              sh  ("""
              curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*Job name - * *${env.JOB_NAME}*, *Build number - * *${BUILD_NUMBER}*, *Testing server address - * *http://${DEV_IP}:8080*' 
              """)
              }
            }
          }
        }


        stage('Configuring Prod Environment with Ansible') {
            // делаю выдачу запроса с ожиданием ответа, типа подтвердить или опровергнуть, на настройку второго сервера
          input{
             message "Push app to Production?"
          }
          steps {
            dir ('ansible') {
              // та же самая настройка
              sh 'ansible-playbook prodenv.yml'
            }
          }
          // репорт с адресом в телегу через бота
          post {
            always {
              withCredentials([string(credentialsId: 'botSecret', variable: 'TOKEN'), string(credentialsId: 'chatId', variable: 'CHAT_ID')]) {
              sh  ("""
              curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*Job name - * *${env.JOB_NAME}*, *Build number - * *${BUILD_NUMBER}*, *Testing server address - * *http://${PROD_IP}:8080*' 
              """)
              }
            }
          }
        }
        stage('Destroy Environment') {
            // опять таки запрос действий
          input{
             message "Do you want to Destroy Dev Environment?"
          }
          steps {
            dir ('terraform') {
                //  уничтожаю всё инфраструктуру, чтобы денежку не кушала, так как это учебный проект, а не продакшн.
              withAWS(credentials: 'AWS_CREDS', region: 'eu-central-1') {
                sh 'terraform destroy --auto-approve'
              }
            }
          }
        }
    }
    // отчитываюсь обо всём в телегу - состояние пайплайна. Варианта три - всё норм, отмена или факап.
    post {
     success { 
        withCredentials([string(credentialsId: 'botSecret', variable: 'TOKEN'), string(credentialsId: 'chatId', variable: 'CHAT_ID')]) {
        sh  ("""
            curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* : POC *Branch*: ${env.GIT_BRANCH} *Build* : OK *Published* = YES'
        """)
        }
     }

     aborted {
        withCredentials([string(credentialsId: 'botSecret', variable: 'TOKEN'), string(credentialsId: 'chatId', variable: 'CHAT_ID')]) {
        sh  ("""
            curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* : POC *Branch*: ${env.GIT_BRANCH} *Build* : `Aborted` *Published* = `Aborted`'
        """)
        }
     
     }
     failure {
        withCredentials([string(credentialsId: 'botSecret', variable: 'TOKEN'), string(credentialsId: 'chatId', variable: 'CHAT_ID')]) {
        sh  ("""
            curl -s -X POST https://api.telegram.org/bot${TOKEN}/sendMessage -d chat_id=${CHAT_ID} -d parse_mode=markdown -d text='*${env.JOB_NAME}* : POC  *Branch*: ${env.GIT_BRANCH} *Build* : `not OK` *Published* = `no`'
        """)
        }
     }

    }
}


```
